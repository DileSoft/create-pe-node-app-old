#Protopia Ecosystem#

Основной сервер для Protopia Ecosystem

## Установка ##

1. Установите СУБД mongodb (например используя инструкцию с https://docs.mongodb.com/manual/installation/)
2. Создайте Базу Данных с названием проекта: **project_name**: `use project_name`
3. Установите необходимые пакеты, выполнив в корневой директории команду: 
`npm install`
4. Сформируйте конфигурационные файлы командой
`npm run pe-config`
5. При выполнении команды введите название базы: **project_name**, HTTP host (по умолчанию `localhost`), HTTP port (по умолчанию `9095`) и Application type (по умолчанию `server`)
6. Настройте сервер и установите модули командой
`npm run pe-install`
7. При выполнении команды скрипт спросит подтверждение на внесение изменений в базу (будут удалены предыдущие данные коллекций `user` и `client`)
8. запустите сервер командой: `npm start` или, при наличии утилиты `pm2` командой `pm2 start npm --name "project_name" -- start`
9. Доступ к GraphQL-консоли для тестирования команд:
    http://localhost:9095/graphql
10. Скопируйте файл из `server/config/client/config.json` в `src/config/config.json` react-клиента
11. Войдите как пользователь с email `admin@protopia-ecosystem.net` и паролем `admin`
